flake8==3.9.1
flake8-django==1.1.1
pep8-naming==0.11.1
flake8-simplify==0.14.0
flake8-bugbear==21.4.3
flake8-comprehensions==3.4.0
flake8-eradicate==1.0.0
flake8-mutable==1.2.0
flake8-expression-complexity==0.0.9
flake8-cognitive-complexity==0.1.0
# flake8-multiline-containers==0.0.18 внедрить позже

